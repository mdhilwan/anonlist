/**
 * Octus Uber List Module
 *
 * Just a simple list using Angular 1
 */
var app = angular.module("UberList", ["firebase"]);

var firebaseUrl = "https://flickering-inferno-2465.firebaseio.com/";
var angularUrl = firebaseUrl + 'angular';
var presenceUrl = firebaseUrl + 'presence';
var connectedUrl = firebaseUrl + '.info/connected';

app.controller("ProfileCtrl", 
    function($scope, $firebaseArray) {

    	var ref = new Firebase(angularUrl);
        var messageTxt;

        $scope.sortMessage = true;
        $scope.sortMethod = 'text';

        $scope.messages = $firebaseArray(ref);

        $scope.sortBtnDown = function(evt) {
            var me = $(evt.target);

            if (!$('.btn-sort-down').hasClass('active') && $('.btn-sort-up').hasClass('active')) {
                $('.btn-sort-down').addClass('active');
                $('.btn-sort-up').removeClass('active');
            } else if ($('.btn-sort-down').hasClass('active') && !$('.btn-sort-up').hasClass('active')) {
                $('.btn-sort-down').removeClass('active');
                $('.btn-sort-up').removeClass('active');
            } else if (!$('.btn-sort-down').hasClass('active') && !$('.btn-sort-up').hasClass('active')) {
                $('.btn-sort-down').addClass('active');
                $('.btn-sort-up').removeClass('active');
            }

            sortList();
        }

        $scope.sortBtnUp = function(evt) {
            var me = $(evt.target);

            if (!$('.btn-sort-up').hasClass('active') && $('.btn-sort-down').hasClass('active')) {
                $('.btn-sort-up').addClass('active');
                $('.btn-sort-down').removeClass('active');
            } else if ($('.btn-sort-up').hasClass('active') && !$('.btn-sort-down').hasClass('active')) {
                $('.btn-sort-up').removeClass('active');
                $('.btn-sort-down').removeClass('active');
            } else if (!$('.btn-sort-up').hasClass('active') && !$('.btn-sort-down').hasClass('active')) {
                $('.btn-sort-up').addClass('active');
                $('.btn-sort-down').removeClass('active');
            }

            sortList()
        }

        function sortList() {

            console.log('sortList')

            if ($('.btn-sort-up').hasClass('active') || $('.btn-sort-down').hasClass('active')) {
                $scope.sortMessage = true;
            } else if (!$('.btn-sort').hasClass('active')) {
                $scope.sortMessage = false;
            }

            if ($scope.sortMessage) {
                if ($('.btn-sort-up').hasClass('active')) {
                    $scope.sortMethod = 'text';
                } else if ($('.btn-sort-down').hasClass('active')) {
                    $scope.sortMethod = '-text';
                } 
            } else {
                $scope.sortMethod = '';
            }
        }

        $scope.toggleEditMessage = function(evt) {
            $(evt.target).parents('.input-group').find('.delete-btn').toggleClass('hidden');
        	$(evt.target).parents('.input-group').find('.message-edit').toggleClass('hidden');
        	$(evt.target).parents('.input-group').find('.message-text').toggleClass('hidden');
        	$(evt.target).parents('.input-group').find('.edit-btn').toggleClass('hidden');
        	$(evt.target).parents('.input-group').find('.save-btn').toggleClass('hidden');
        }

        $scope.addMessage = function() {

            if ($('.add-message').val() == '' || !$('.add-message').val() || $.trim( $('.add-message').val() ) == '') {
                alert('Please key in something!')
                return false;
            }

            $scope.messages.$add({
            	text: $scope.newMessageText
            })

            $('.add-message').val('');
        };

        var listRef = new Firebase(presenceUrl);
        var userRef = listRef.push();

        // Add ourselves to presence list when online.
        var presenceRef = new Firebase(connectedUrl);
        presenceRef.on("value", function(snap) {
            if (snap.val()) {
                // Remove ourselves when we disconnect.
                userRef.onDisconnect().remove();

                userRef.set(true);
            }
        });

        $scope.onlineUser = 1;

        // Number of online users
        listRef.on("value", function(snap) {
            $scope.onlineUser = snap.numChildren();
        });
  
    }
);
