// Backbone View for all list will point to $('.list-coll')
app.view.AppView = Backbone.View.extend({

    model: app.firebase.lists,
    // model: app.collection.lists, <-- Local Version

    el: $('#app-view'),

    initialize: function() {
        this.model.on('add', this.render, this);
        this.model.on('change', this.render, this);
        this.model.on('remove', this.render, this);
    },

    render: function() {
        var self = this;

        self.$el.find('.list').html('');
        
        _.each(this.model.toArray(), function(_list){

            self.$el.find('.list').append(
                (new app.view.List({
                    model: _list
                })).render().$el
            )
        });

        return this;
    }
});

var appView = new app.view.AppView();