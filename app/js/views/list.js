// Backbone View for one list
app.view.List = Backbone.View.extend({

    model: new app.model.List,

    tagName: 'div',

    className: 'input-group message-group',

    initialize: function() {
        this.template = _.template($('.list-template').html());
    },

    events: {
        'click .edit-btn': 'toggleEdit',
        'dblclick .message-text': 'toggleEdit',
        'click .save-btn': function(evt) {
            this.saveBtnClick(evt);
            this.toggleEdit(evt);
        },
        'click .delete-btn': 'delBtnClick'
    },

    toggleEdit: function(evt) {
        var listView = $(evt.target).parents('.message-group');
        msgTxt = listView.find('.message-text'),
            msgEdit = listView.find('.message-edit'),
            saveBtn = listView.find('.save-btn'),
            delBtn = listView.find('.delete-btn');

        msgTxt.toggleClass('hidden');
        msgEdit.toggleClass('hidden');
        saveBtn.toggleClass('hidden');
        delBtn.toggleClass('hidden');
    },

    saveBtnClick: function() {
        this.model.set('text', msgEdit.val());
    },

    delBtnClick: function(evt) {
        this.model.destroy();
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));

        return this;
    }
})