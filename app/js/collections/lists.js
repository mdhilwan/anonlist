// Lists Collection
app.collection.Lists = Backbone.Collection.extend({});

// Instantiate Lists Collection
app.collection.lists = new app.collection.Lists();

// Firebase MessageList
app.firebase.Lists = Backbone.Firebase.Collection.extend({
	url: backboneUrl,
	autoSync: true
})

// Instantiate Firebase MessageList
app.firebase.lists = new app.firebase.Lists();