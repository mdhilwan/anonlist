var app = {
	firebase: {},
    model: {},
    view: {},
    collection: {}
}

var firebaseUrl = "https://flickering-inferno-2465.firebaseio.com/";
var backboneUrl = firebaseUrl + 'backbone';
var presenceUrl = firebaseUrl + 'presence';
var connectedUrl = firebaseUrl + '.info/connected';

$(document).ready(function() {

    $('.add-list').click(function(event) {

        var list = new app.model.List({
            text: $('.list-name').val()
        })

        // app.collection.lists.add(list); <-- Local version
        app.firebase.lists.add(list.toJSON()); // <-- Firebase version

        $('.list-name').val('');
    });
});
