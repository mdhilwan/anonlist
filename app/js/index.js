var firebaseUrl = "https://flickering-inferno-2465.firebaseio.com/";

angular.module('messageBoard', ['firebase'])
	.controller('MessageController', 
		function($scope, $firebaseObject){

			var firebaseRef = new Firebase(firebaseUrl);

			$scope.messages = $firebaseObject(firebaseRef);	

			$scope.messages.$bindTo($scope, 'data')
		})