var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var cssmin = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

/**
 * Launch the Server
 */
gulp.task('browser-sync', ['sass', 'scripts'], function() {
    browserSync.init(null, {
        // Change as required
        proxy: "localhost:8888/octus/app",
        socket: {
            // For local development only use the default BrowserSync local URL.
            // domain: 'localhost:3000',
            // For external development (e.g on a mobile or tablet) use an external URL.
            // You will need to update this to whatever BS tells you is the external URL when you run Gulp.
            domain: 'localhost:3000'
        }
    });
});

/**
 * Compile files from scss
 */
gulp.task('sass', function() {
    return gulp.src(['app/scss/style.scss'])
        .pipe(sass({
            includePaths: ['scss'],
            onError: browserSync.notify
        }))
        .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
            cascade: true
        }))
        .pipe(gulp.dest('app/css/'))
        .pipe(browserSync.reload({
            stream: true
        }))
});


/**
 * Minify js scripts
 */
gulp.task('scripts', function() {
    return gulp.src('app/js/source/*.js')
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('app/js/'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

/**
 * Reload page when views change
 */
gulp.task('views', function() {
    browserSync.reload();
});

/**
 * Watch scss files for changes & recompile
 * Watch views folder for changes and reload BrowserSync
 */
gulp.task('watch', function() {
    gulp.watch(['app/scss/style.scss'], ['sass']);
    gulp.watch(['app/js/source/*.js'], ['scripts']);
    gulp.watch(['app/*.html'], ['views']);
});

/**
 * Default task, running just `gulp` will compile the sass,
 * compile the scripts, launch BrowserSync & watch files.
 */
gulp.task('default', ['browser-sync', 'watch']);
gulp.task('build', ['sass-prod']);
