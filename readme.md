# Anon List

Working on a list using Angular.js and also Backbone.js. Basically trying to understand the whole MV*/MVC/MVVM etc framework using javascript.

###Frontend:
- jQuery
- Bootstrap
- Underscore
- Angular (1.5.0)
- Backbone (1.2.3)

###Backend
- Firebase (2.4.0)
- BackboneFire (0.5.1)
- AngularFire (1.1.3)

Bootstrap for the frontend styling, Firebase for the async realtime database, Angularfire.js / Backbonefire.js to connect with the Firebase.

###Objective:
- Make list:
    + Able to "add", "delete", "edit" list item
    + Editable via double clicking
    + Sortable
    + Searchable

###Built With:
- [AngularJS 1 + Firebase + AngularFire](http://list.col-md-12.com/index-angular.html)
- [Backbone + Firebase + BackboneFire](http://list.col-md-12.com/index-backbone.html)
